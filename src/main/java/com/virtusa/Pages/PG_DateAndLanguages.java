package com.virtusa.Pages;

import org.openqa.selenium.By;

public class PG_DateAndLanguages {

    public static By btn_DateAndLanguages= By.xpath("//button[@title='Date & Languages']");
    public static By btn_DocumentDisplyLanguage=By.cssSelector("lnip-interface-display-language-section:nth-child(1) .radiobutton-group-options:nth-child(1) > .ng-star-inserted:nth-child(1) .radiobutton-label:nth-child(2)");
    public static By btn_searchLanguages= By.cssSelector("lnip-document-display-language:nth-child(2) .ng-star-inserted:nth-child(2) .radiobutton-label:nth-child(2)");
    public static By btn_DateFormat= By.cssSelector("lnip-date-format:nth-child(1) .radiobutton-group-options:nth-child(1) > .ng-star-inserted:nth-child(1) .radiobutton-label:nth-child(2)");
    public static By btn_DateSeperator= By.cssSelector("lnip-date-separator:nth-child(2) .ng-star-inserted:nth-child(2) .radiobutton-label:nth-child(2)");
    public static By btn_DisplayTime= By.cssSelector("lnip-display-time:nth-child(3) .ng-star-inserted:nth-child(2) .radiobutton-label:nth-child(2)");
    public static By btn_SaveAndClose= By.cssSelector(".button > span");
}
