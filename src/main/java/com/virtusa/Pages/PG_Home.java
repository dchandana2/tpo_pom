package com.virtusa.Pages;

import org.openqa.selenium.By;

public class PG_Home {

    public static By btn_Ustertest =By.cssSelector(".drop-down-wrapper > .icon-user-preferences");
    public static By btn_Preferences =By.cssSelector(".icon-lg");
    public static By btn_Signout = By.xpath("//button[@title='Sign out']");

    public static By tf_SearchBox = By.cssSelector(".object-search-text-editor");

    public static By btn_SearchBtn = By.cssSelector(".object-search-search-btn .ng-star-inserted");
}
