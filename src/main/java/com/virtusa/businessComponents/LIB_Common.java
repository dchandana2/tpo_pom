package com.virtusa.businessComponents;

import com.virtusa.Pages.PG_Home;
import com.virtusa.Pages.PG_Login;
import com.virtusa.util.WebdriverUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;


public class LIB_Common extends WebdriverUtil{
    public static void bc_Login(){

        WebdriverUtil.openBrowser();
        driver.get("https://passive.totalpatentone.com");


        String username = null;
        String password = null;


        try {
            FileInputStream the_file = new FileInputStream(new File("C:\\Users\\dkodithuwakku\\Downloads\\Data.xlsx"));
            XSSFWorkbook workbook = new XSSFWorkbook(the_file);
            XSSFSheet sheet = workbook.getSheetAt(0);
            Row first_row = sheet.getRow(0);
            //String u_name = first_row.getCell(0);
            username = String.valueOf(sheet.getRow(0).getCell(0));
            password = String.valueOf(sheet.getRow(0).getCell(1));

        } catch (Exception e) {
            e.printStackTrace();
        }


        driver.findElement(PG_Login.tf_Username).sendKeys(username);
        driver.findElement(PG_Login.tf_Password).sendKeys(password);
        driver.findElement(PG_Login.btn_Login).click();
    }

    public static void bc_Logout(){
        driver.findElement(PG_Home.btn_Ustertest).click();
        driver.findElement(PG_Home.btn_Signout).click();
    }

    public static void bc_Search(){
        driver.findElement(PG_Home.tf_SearchBox).sendKeys("Car");

        driver.findElement(PG_Home.btn_SearchBtn).click();
    }

}
