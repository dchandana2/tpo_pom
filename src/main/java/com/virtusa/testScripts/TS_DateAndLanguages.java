package com.virtusa.testScripts;

import com.virtusa.Pages.PG_DateAndLanguages;
import com.virtusa.Pages.PG_Home;
import com.virtusa.businessComponents.LIB_Common;
import com.virtusa.businessComponents.LIB_Home;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import static com.virtusa.util.WebdriverUtil.driver;

public class TS_DateAndLanguages {

    @AfterTest
    public void tearDown() {
        driver.quit();
    }

    @Test
    public static void tc_ValidateDateAndLanguages() throws InterruptedException {
        LIB_Common.bc_Login();
        LIB_Home.bc_SelectTabFromHome();
        Thread.sleep(2000);
        driver.findElement(PG_Home.btn_Preferences).click();
        Thread.sleep(2000);

        driver.findElement(PG_DateAndLanguages.btn_DateAndLanguages).click();
        driver.findElement(PG_DateAndLanguages.btn_DocumentDisplyLanguage).click();
        driver.findElement(PG_DateAndLanguages.btn_searchLanguages).click();
        driver.findElement(PG_DateAndLanguages.btn_DateFormat).click();
        driver.findElement(PG_DateAndLanguages.btn_DateSeperator).click();
        driver.findElement(PG_DateAndLanguages.btn_DisplayTime).click();
        Thread.sleep(2000);
        driver.findElement(PG_DateAndLanguages.btn_SaveAndClose).click();
        Thread.sleep(3000);

        LIB_Common.bc_Logout();


    }
}
