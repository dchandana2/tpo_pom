package com.virtusa.testScripts;

import com.virtusa.Pages.PG_Home;
import com.virtusa.businessComponents.LIB_Common;
import com.virtusa.businessComponents.LIB_Home;
import org.openqa.selenium.By;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import static com.virtusa.util.WebdriverUtil.driver;

public class TS_QueryAndResults {

    @AfterTest
    public void tearDown() {
        driver.quit();
    }
    @Test
    public static void tc_ValidateQueryAndResults() throws InterruptedException {

        LIB_Common.bc_Login();
        LIB_Home.bc_SelectTabFromHome();
        Thread.sleep(2000);
        driver.findElement(PG_Home.btn_Preferences).click();
        Thread.sleep(2000);

        Thread.sleep(5000);
        //driver.findElement(By.cssSelector(".icon-lg")).click();
        driver.findElement(By.xpath("//button[@title='Query and results']"));
        Thread.sleep(2000);
        ////button[@data-testid='drop-down-button-content' and @title='Results per page']
        driver.findElement(By.cssSelector(".select-dropdown .drop-down-button-content")).click();
        Thread.sleep(5000);
        driver.findElement(By.cssSelector(".drop-down-list-item:nth-child(3) .drop-down-list-item-link")).click();
        Thread.sleep(5000);

        //button[@title='SAVE AND CLOSE']
        driver.findElement(By.xpath("//button[@title='SAVE AND CLOSE']")).click();
        Thread.sleep(5000);

    LIB_Common.bc_Logout();
    }
}
