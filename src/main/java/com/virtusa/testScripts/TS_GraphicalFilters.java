package com.virtusa.testScripts;

import com.virtusa.businessComponents.LIB_Common;
import com.virtusa.businessComponents.LIB_Home;
import org.openqa.selenium.By;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import static com.virtusa.util.WebdriverUtil.driver;

public class TS_GraphicalFilters {
    @AfterTest
    public void tearDown() {
        driver.quit();
    }

    @Test
    public static void tc_ValidateTextGraphicalFilters() throws InterruptedException {

        LIB_Common.bc_Login();
        LIB_Common.bc_Search();

        Thread.sleep(8000);


        driver.findElement(By.xpath("//button[@data-testid='icon-filter-graphical']")).click();

        Thread.sleep(8000);

        driver.findElement(By.id("labelgfPC0")).click();
        Thread.sleep(8000);
        driver.findElement(By.id("labelgfPC0")).click();
        Thread.sleep(8000);
        driver.findElement(By.id("labelgfPAS0")).click();
        Thread.sleep(8000);
        driver.findElement(By.id("labelgfPAS0")).click();
        Thread.sleep(8000);
        driver.findElement(By.id("labelgfIN0")).click();
        Thread.sleep(8000);
        driver.findElement(By.id("labelgfIN0")).click();
        Thread.sleep(8000);
        driver.findElement(By.id("labelgfCPC0")).click();
        Thread.sleep(8000);
        driver.findElement(By.id("labelgfCPC0")).click();
        Thread.sleep(8000);
        driver.findElement(By.id("labelgfIPC2")).click();
        Thread.sleep(8000);
        driver.findElement(By.id("labelgfIPC2")).click();
        Thread.sleep(8000);
        driver.findElement(By.id("labelgfPRD4")).click();
        Thread.sleep(8000);
        driver.findElement(By.id("labelgfPRD4")).click();
        Thread.sleep(8000);
        driver.findElement(By.id("labelgfPD6")).click();
        Thread.sleep(8000);
        driver.findElement(By.id("labelgfPD6")).click();
        Thread.sleep(8000);
        driver.findElement(By.id("labelgfAD7")).click();
        Thread.sleep(8000);
        driver.findElement(By.id("labelgfAD7")).click();
        Thread.sleep(8000);

        LIB_Common.bc_Logout();

    }
}
