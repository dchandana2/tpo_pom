package com.virtusa.testScripts;

import com.virtusa.businessComponents.LIB_Common;
import org.openqa.selenium.By;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import static com.virtusa.util.WebdriverUtil.driver;

public class TS_Export {

    @AfterTest
    public void tearDown() {
        driver.quit();
    }

    @Test
    public static void tc_ValidateFieldSelectionInExport() throws InterruptedException {

        LIB_Common.bc_Login();
        LIB_Common.bc_Search();

        Thread.sleep(8000);

        driver.findElement(By.xpath("(//label[@class='checkbox-label'])[5]")).click();
        Thread.sleep(1000);
        driver.findElement(By.xpath("(//label[@class='checkbox-label'])[6]")).click();
        Thread.sleep(1000);
        driver.findElement(By.xpath("(//label[@class='checkbox-label'])[7]")).click();
        Thread.sleep(1000);
        driver.findElement(By.cssSelector(".mat-menu-trigger")).click();
        Thread.sleep(1000);
        driver.findElement(By.cssSelector(".mat-focus-indicator")).click();
        Thread.sleep(1000);
        driver.findElement(By.cssSelector(".ng-star-inserted:nth-child(4) > lnip-radiobutton .radiobutton-label")).click();
        Thread.sleep(1000);
        driver.findElement(By.cssSelector(".ng-star-inserted:nth-child(4) > .radiobutton-group-options > .ng-star-inserted:nth-child(2) .radiobutton-label")).click();
        Thread.sleep(1000);
        driver.findElement(By.cssSelector(".ng-star-inserted > .radiobutton-group-option:nth-child(1) > .radiobutton-label")).click();
        Thread.sleep(1000);
        driver.findElement(By.cssSelector(".ng-star-inserted > .radiobutton-group-option:nth-child(2) > .radiobutton-label")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("customExportName")).sendKeys("Docs");
        Thread.sleep(1000);
        driver.findElement(By.linkText("Next")).click();
        Thread.sleep(5000);
        driver.findElement(By.xpath("//label[@id=\'labelselectAllFields\']")).click();
        Thread.sleep(1000);
        driver.findElement(By.xpath("//label[@id=\'labelselectAllFields\']")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("labelselect_all_dates_numbers")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("labelselect_all_dates_numbers")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("labelselect_all_text")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("labelselect_all_text")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("labelselect_all_persons")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("labelselect_all_persons")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("labelselect_all_legal")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("labelselect_all_legal")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("labelselect_all_family_citations")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("labelselect_all_family_citations")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("labelselect_all_classifications")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("labelselect_all_classifications")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("labelselect_all_pharma")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("labelselect_all_pharma")).click();
        Thread.sleep(1000);
        driver.findElement(By.cssSelector(".export-modal-footer-button:nth-child(2)")).click();
        Thread.sleep(5000);

        LIB_Common.bc_Logout();
    }

}
