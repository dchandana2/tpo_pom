package com.virtusa.util;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class WebdriverUtil {
    public static WebDriver driver;
    public static Map<String, Object> vars;
    static JavascriptExecutor js;

    public static WebDriver getDriver() {
        return driver;
    }

    public static void setDriver(WebDriver driver) {
        WebdriverUtil.driver = driver;
    }

    public static void openBrowser(){
        js = (JavascriptExecutor) driver;
        vars = new HashMap<String, Object>();

        driver=new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().deleteAllCookies();
        driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
    }

}
